package follower;
public class follower {
	Vector position = new Vector(0,0);
	Vector velocity = new Vector(0,0);
	Vector acceleration = new Vector(0,0);
	int r;
	float mass, maxSpeed, maxForce;
	public follower(float x, float y)
	{
		this.position.x = x;
		this.position.y = y;
		this.mass = 1;
		this.r = 10;
		this.maxSpeed = 20;
		this.maxForce = (float)3;
	}
	public void update()
	{
		this.position.add(velocity);
		this.velocity.add(acceleration);
		this.velocity.limit(maxSpeed);
		this.acceleration.mult(0);
	}
	public void eat(Vector food[])
	{
		Vector record_food = food[0].copy(); 
		float recordDist = 100;
		int recordIndex = 0 ;
		for(int i = 0; i<food.length;i++)
		{
			if(Vector.dist(this.position, food[i])<=Vector.dist(record_food, this.position))
			{
				record_food = food[i].copy();
				recordDist = Vector.dist(record_food, this.position);
				recordIndex = i;
			}
		}
		if(recordDist<10)
		{
			food[recordIndex] = Vector.random(1000, 1000, 10);
		}
		//System.out.println(record_food);
		this.seek(record_food);
	}
	
	public void seek(Vector target)
	{
		Vector desired  = Vector.sub(target, position);
		desired.setMag(this.maxSpeed);
		Vector steer = Vector.sub(desired, this.velocity);
		steer.limit(this.maxForce);
		this.applyForce(steer);
	}
	public void applyForce(Vector force)
	{
		force.div(mass);
		acceleration.add(force);
	}
}
