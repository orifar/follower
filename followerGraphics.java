package follower;
import javax.swing.*;
import java.awt.Graphics;
import java.awt.Color;
@SuppressWarnings("serial")
public class followerGraphics extends JFrame
{
	follower fo;
	Vector food[];
	int foodR = 10;
	public followerGraphics(int WIDTH, int HEIGHT, follower fo, Vector food[])
	{
		DrawPane panel1 = new DrawPane(WIDTH, HEIGHT);
		this.getContentPane().add(panel1);
		this.fo = fo;
		this.food = food.clone();
		
		setSize(WIDTH, HEIGHT);
		setVisible(true);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	class DrawPane extends JPanel 
	{
		int WIDTH;
		int HEIGHT;
		public DrawPane(int WIDTH, int HEIGHT)
		{
			this.WIDTH = WIDTH;
			this.HEIGHT = HEIGHT;
		}
		public void paintComponent(Graphics g)
		{
			super.paintComponent(g);
			g.drawOval(Math.round(fo.position.x)+fo.r, Math.round(fo.position.y)+fo.r, fo.r, fo.r);
			for(Vector piece:food)
			{
				g.fillOval(Math.round(piece.x)+foodR,Math.round(piece.y)+foodR,foodR,foodR);
			}
		}
	}
}
