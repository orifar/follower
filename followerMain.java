package follower;

public class followerMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int WIDTH  = 1000;
		int HEIGHT = 1000;
		Vector food[] = new Vector[50];
		follower fo = new follower(WIDTH/2, HEIGHT/2);
		
		for(int i = 0; i<food.length; i++)
		{
			food[i] = Vector.random(WIDTH, HEIGHT, 10);
		}
		followerGraphics graph = new followerGraphics(WIDTH, HEIGHT, new follower(fo.position.x, fo.position.y),food.clone());
		
		while(true)
		{
			try        
			{
			    Thread.sleep(50);
			} 
			catch(InterruptedException ex) 
			{
			    Thread.currentThread().interrupt();
			}
			graph.food = food.clone();
			fo.update();
			fo.eat(food);
			graph.fo = new follower(fo.position.x, fo.position.y);
			graph.getContentPane().repaint();
			//System.out.println(fo.position);
			//System.out.println(fo.velocity);
			//System.out.println(fo.acceleration);
		}
	}
}
