package follower;
public class Vector {
	public float x;
	public float y;
	public Vector(float x, float y)
	{
		this.x = x;
		this.y = y;
	}
	public void set(float x, float y)
	{
		this.x = x;
		this.y = y;
	}
	public void set(Vector v)
	{
		this.x = v.x;
		this.y = v.y;
	}
	public void set(float[] source)
	{
		this.x = source[0];
		this.y = source[1];
	}
	public static  Vector random(int width, int height, float lower)
	{
		float randX = (float)(Math.random()*(width-lower)+lower);
		float randY = (float)(Math.random()*(height-lower)+lower);
		return(new Vector(randX, randY));
	}
	public Vector copy()
	{
		return(new Vector(this.x,this.y));
	}
	public float mag()
	{
		return((float) Math.sqrt(Math.pow(x,2)+ Math.pow(y,2)));
	}
	public void add(Vector v)
	{
		this.x+=v.x;
		this.y+=v.y;
	}
	
	public static Vector add(Vector v1, Vector v2)
	{
		return(new Vector(v1.x+v2.x, v1.y+v2.y));
	}
	
	public void sub(Vector v)
	{
		this.x-=v.x;
		this.y-=v.y;
	}
	
	public static Vector sub(Vector v1, Vector v2)
	{
		return(new Vector(v1.x-v2.x, v1.y-v2.y));
	}
	
	public void mult(float scalar)
	{
		this.x*=scalar;
		this.y*=scalar;
	}
	
	public static Vector mult(Vector v, float scalar)
	{
		return(new Vector(v.x*scalar, v.y*scalar));
	}
	
	public void div(float scalar)
	{
		this.x=(float)(this.x/scalar);
		this.y=(float)(this.y/scalar);
	}
	
	public static Vector div(Vector v, float scalar)
	{
		return(new Vector(v.x/scalar, v.y/scalar));
	}
	public float dist(Vector v)
	{
		return (float) Math.abs((Math.sqrt(Math.pow(v.x-this.x, 2) + Math.pow(v.y-this.y, 2))));
	}
	public static float dist(Vector v1, Vector v2)
	{
		return (float) (Math.sqrt(Math.pow(v2.x-v1.x, 2) + Math.pow(v2.y-v1.y, 2)));
	}
	public float getMag()
	{
		return (float) (Math.sqrt(Math.pow(this.x,2)+Math.pow(this.y,2)));
	}
	public void setMag(float mag)
	{
		float ratio = this.getMag()/mag;
		this.div(ratio);
	}
	public void limit(float mag)
	{
		if(this.getMag()>mag)
		{
			float ratio = this.getMag()/mag;
			this.div(ratio);
		}
	}
	public String toString()
	{
		float[] point = {this.x, this.y};
		String str ="{"+point[0]+", "+point[1]+"}";
		return(str);
	}
}
